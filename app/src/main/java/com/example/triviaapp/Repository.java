package com.example.triviaapp;

import androidx.lifecycle.MutableLiveData;

import com.example.triviaapp.pojo.ColorPojo;
import com.example.triviaapp.pojo.PlayersPojo;

import java.util.ArrayList;
import java.util.List;

public class Repository {

    private static Repository instance;
    private ArrayList<PlayersPojo> playerDataSet = new ArrayList<>();
    private ArrayList<ColorPojo> colorsDataSet = new ArrayList<>();

    public static Repository getInstance() {

        return instance = new Repository();
    }

    public MutableLiveData<List<PlayersPojo>> getBestCricketer() {
        setBestCricketer();
        MutableLiveData<List<PlayersPojo>> data = new MutableLiveData<>();
        data.setValue(playerDataSet);
        return data;
    }

    public MutableLiveData<List<ColorPojo>> getFlagColors() {
        setFlagColors();
        MutableLiveData<List<ColorPojo>> data = new MutableLiveData<>();
        data.setValue(colorsDataSet);
        return data;
    }

    private void setBestCricketer() {
        playerDataSet.add(
                new PlayersPojo("A) Sachin Tendulkar")
        );
        playerDataSet.add(
                new PlayersPojo("B) Virat Kohli")
        );
        playerDataSet.add(
                new PlayersPojo("C) Adam Gilchirst")
        );
        playerDataSet.add(
                new PlayersPojo("D) Jacques Kallis")
        );

    }

    private void setFlagColors() {
        colorsDataSet.add(
                new ColorPojo("A) White")
        );
        colorsDataSet.add(
                new ColorPojo("B) Yellow")
        );
        colorsDataSet.add(
                new ColorPojo("C) Orange")
        );
        colorsDataSet.add(
                new ColorPojo("D) Green")
        );

    }
}