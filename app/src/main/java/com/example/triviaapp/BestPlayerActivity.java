package com.example.triviaapp;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.triviaapp.database.TriviaDb;
import com.example.triviaapp.pojo.PlayersPojo;

import java.util.List;

public class BestPlayerActivity extends AppCompatActivity {

    private MainActivityViewModel mMainActivityViewModel;
    RadioGroup rg;
    RadioButton radioButton;
    RelativeLayout relative_layout;
    Button btn_next;
    TriviaDb triviaDb;
    String str_bestPlayer,strName="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_best_players);

        triviaDb = TriviaDb.getTrivia_dbInstance(this);
        triviaDb.open();

        relative_layout=findViewById(R.id.relative_layout);
        btn_next=findViewById(R.id.btn_next);
        rg = new RadioGroup(this);
        radioButton = new RadioButton(this);

        strName = getIntent().getStringExtra("username");

        mMainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);

        mMainActivityViewModel.init();

        mMainActivityViewModel.getBestCricketer().observe(this, new Observer<List<PlayersPojo>>() {
            @Override
            public void onChanged(@Nullable List<PlayersPojo> playersPojos) {

                Log.e("sharu",""+playersPojos);
                getPlayers(playersPojos);
            }
        });


        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) findViewById(checkedId);
            }
        });


        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // if none of the options are selected
                if (rg.getCheckedRadioButtonId() == -1)
                {
                    Toast.makeText(BestPlayerActivity.this,"Please select any options",Toast.LENGTH_LONG).show();
                }
                else
                {

                    // To get checked radio button text
                    int selectedId = rg.getCheckedRadioButtonId();
                    // find the radiobutton by returned id
                    radioButton = (RadioButton) findViewById(selectedId);
                    str_bestPlayer=radioButton.getText().toString();


                    // updating best player based on username
                    ContentValues update_CV = new ContentValues();
                    update_CV.put(TriviaDb.fldbestcricketer,str_bestPlayer);
                    triviaDb.update(TriviaDb.tbltrivia,update_CV,TriviaDb.fldname + "=?", new String[]{strName});
                    triviaDb.close();

                    startActivity(new Intent(BestPlayerActivity.this, FlagActivity.class).putExtra("username",strName));

                }


            }
        });

    }

    private void getPlayers(List<PlayersPojo> playersPojos) {

        // Creating dynamic radio button
        for(int i=0;i<playersPojos.size();i++) {
            radioButton=new RadioButton(this);
            radioButton.setText(playersPojos.get(i).getName());
            radioButton.setId(i);
            rg.addView(radioButton);
            rg.setOrientation(RadioGroup.VERTICAL);
        }
        relative_layout.addView(rg);

    }

}
