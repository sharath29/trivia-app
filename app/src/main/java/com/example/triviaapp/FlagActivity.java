package com.example.triviaapp;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.triviaapp.database.TriviaDb;
import com.example.triviaapp.pojo.ColorPojo;

import java.util.List;

public class FlagActivity extends AppCompatActivity {

    private MainActivityViewModel mMainActivityViewModel;
    CheckBox checkBox;
    RelativeLayout relative_layout;
    LinearLayout ll;
    Button btn_next;
    String strflagColors = "",strName="";
    TriviaDb triviaDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flag);

        triviaDb = TriviaDb.getTrivia_dbInstance(this);
        triviaDb.open();

        relative_layout = findViewById(R.id.relative_layout);
        ll = (LinearLayout) findViewById(R.id.linearLayout2);
        btn_next = findViewById(R.id.btn_next);

        strName = getIntent().getStringExtra("username");

        mMainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);

        mMainActivityViewModel.initColors();

        mMainActivityViewModel.getFlagColors().observe(this, new Observer<List<ColorPojo>>() {
            @Override
            public void onChanged(@Nullable List<ColorPojo> colorPojos) {

                Log.e("sharu1", "" + colorPojos);
                getColors(colorPojos);
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                


                //getting all child inside linear layout
                for (int i = 0; i < ll.getChildCount(); i++) {
                    View view = ll.getChildAt(i);
                    if (view instanceof CheckBox) {
                        if (((CheckBox) view).isChecked()) {       // Appendind all checked checkbox value
                            strflagColors += ((CheckBox) view).getText().toString() + " ,";
                            Log.e("cb", "" + strflagColors);
                        }

                    }
                }

                if(!strflagColors.equals("")) {

                    // updating best player based on username
                    ContentValues update_CV = new ContentValues();
                    update_CV.put(TriviaDb.fldflagcolor,strflagColors);
                    triviaDb.update(TriviaDb.tbltrivia,update_CV,TriviaDb.fldname + "=?", new String[]{strName});
                    triviaDb.close();

                    startActivity(new Intent(FlagActivity.this, NameHistoryActivity.class));
                    finish();



                } else {
                    Toast.makeText(FlagActivity.this, "Please select any options", Toast.LENGTH_SHORT).show();
                }






            }
        });


    }

    private void getColors(List<ColorPojo> colorPojos) {

        LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        // Creating dynamic Check box
        for (int i = 0; i < colorPojos.size(); i++) {
            checkBox = new CheckBox(this);
            checkBox.setText(colorPojos.get(i).getColor());
            checkBox.setLayoutParams(lparams);
            checkBox.setId(i);   // setting id for checkbox reference
            ll.addView(checkBox);

        }
        //    relative_layout.addView(ll);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
