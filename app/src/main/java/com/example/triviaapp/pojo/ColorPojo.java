package com.example.triviaapp.pojo;

public class ColorPojo {

    private String color;

    public ColorPojo() {
    }

    public ColorPojo(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
