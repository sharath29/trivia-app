package com.example.triviaapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.triviaapp.R;
import com.example.triviaapp.pojo.HistoryPojo;

import java.util.ArrayList;
import java.util.List;

public class HistoryRecyclerAdapter extends RecyclerView.Adapter<HistoryRecyclerAdapter.MyViewHolder> {

    List<HistoryPojo> historyPojoArrayList = new ArrayList<>();
    Context context;

    public HistoryRecyclerAdapter(Context context, List<HistoryPojo> historyPojoArrayList) {
        this.context = context;
        this.historyPojoArrayList = historyPojoArrayList;
    }

    @NonNull
    @Override
    public HistoryRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_details_cards, parent, false);
        HistoryRecyclerAdapter.MyViewHolder mv = new HistoryRecyclerAdapter.MyViewHolder(v);
        return mv;
    }

    @Override
    public void onBindViewHolder(HistoryRecyclerAdapter.MyViewHolder myViewHolder, int i) {

        final HistoryPojo historyPojo = historyPojoArrayList.get(i);

        myViewHolder.tv_game.setText("GAME "+historyPojo.getSlNo());
        myViewHolder.tv_date.setText(" : "+historyPojo.getDate());
        myViewHolder.tv_name.setText("Name : " + historyPojo.getUsername());
        myViewHolder.tv_q1_ans.setText("Answer : " + historyPojo.getBestPlayer());
        myViewHolder.tv_q2_ans.setText("Answers : " + historyPojo.getFlagColors());
    }

    @Override
    public int getItemCount() {
        return historyPojoArrayList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_game,tv_date,tv_name;
        public TextView tv_q1_ans,tv_q2_ans;


        public MyViewHolder(View itemView) {
            super(itemView);

            tv_game = itemView.findViewById(R.id.tv_game_no);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_q1_ans = itemView.findViewById(R.id.tv_question1_ans);
            tv_q2_ans = itemView.findViewById(R.id.tv_question2_ans);

        }
    }

}
