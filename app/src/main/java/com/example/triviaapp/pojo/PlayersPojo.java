package com.example.triviaapp.pojo;

public class PlayersPojo {
    private String name;

    public PlayersPojo() {
    }

    public PlayersPojo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
