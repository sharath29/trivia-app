package com.example.triviaapp.pojo;

public class HistoryPojo {

    private int slNo;
    private String username;
    private String bestPlayer;
    private String flagColors;
    private String date;

    public HistoryPojo(int slNo, String username, String bestPlayer, String flagColors, String date) {
        this.slNo = slNo;
        this.username = username;
        this.bestPlayer = bestPlayer;
        this.flagColors = flagColors;
        this.date = date;
    }

    public int getSlNo() {
        return slNo;
    }

    public void setSlNo(int slNo) {
        this.slNo = slNo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBestPlayer() {
        return bestPlayer;
    }

    public void setBestPlayer(String bestPlayer) {
        this.bestPlayer = bestPlayer;
    }

    public String getFlagColors() {
        return flagColors;
    }

    public void setFlagColors(String flagColors) {
        this.flagColors = flagColors;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
