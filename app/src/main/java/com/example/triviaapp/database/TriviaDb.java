package com.example.triviaapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class TriviaDb {

    public final static String DATABASE_NAME="Trivia_Db";
    public final static int DATABASE_VERSION=1;


    SQLiteDatabase sqLiteDatabase;
    SQLiteHelper sqLiteHelper;

    public static TriviaDb triviaDb;
    public static Context con;

    public final static String tbltrivia = "tbltrivia",
            fldslno = "fldslno",
            fldname = "fldname",
            fldbestcricketer = "fldbestcricketer",
            fldflagcolor = "fldflagcolor",
            flddate = "flddate";


    public static TriviaDb getTrivia_dbInstance(Context context) {
        con=context;
        if(triviaDb == null) {
            triviaDb=new TriviaDb();
        }
        return triviaDb;
    }



    public TriviaDb() {
        sqLiteHelper=new SQLiteHelper(con,DATABASE_NAME,null,DATABASE_VERSION);
    }

    public void open() {
        sqLiteDatabase=sqLiteHelper.getWritableDatabase();
    }

    public void close() {

        if(triviaDb != null) {
            if(sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
                triviaDb=null;
            }
        }

    }


    public class SQLiteHelper extends SQLiteOpenHelper {

        public SQLiteHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL("CREATE TABLE IF NOT EXISTS " + tbltrivia +"(" +fldslno +" integer primary key ,"+
                    fldname + " text ," +
                    fldbestcricketer + " text ," +
                    fldflagcolor + " text ," +
                    flddate + " text )"
            );
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

    public long insert(ContentValues cv, String tablename) {
        long rowid=0;

        try {
            rowid=sqLiteDatabase.insert(tablename,null,cv);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rowid;
    }

    public long update(String table, ContentValues values, String whereClause, String[] whereArgs)
    {
        return sqLiteDatabase.update(table, values, whereClause, whereArgs);
    }

    public Cursor getCompleteHistory(){
        Cursor cursor = null;
        cursor=  sqLiteDatabase.rawQuery("select * from "+tbltrivia   ,null);
        return cursor;
    }

    public Cursor getUsernameCount(String username)
    {
        Cursor cursor = sqLiteDatabase.rawQuery("select count(*) from " + tbltrivia + " where " + fldname + "=? " , new String[]{username});
        return cursor;
    }

}
