package com.example.triviaapp;

import android.database.Cursor;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.triviaapp.adapter.HistoryRecyclerAdapter;
import com.example.triviaapp.database.TriviaDb;
import com.example.triviaapp.pojo.HistoryPojo;

import java.util.ArrayList;

public class HistoryActivity extends AppCompatActivity {


    ArrayList<HistoryPojo> historyPojoArrayList = new ArrayList<>();
    TriviaDb triviaDb;
    HistoryRecyclerAdapter historyRecyclerAdapter;
    public RecyclerView recyclerView;
    public LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        initilization();

        getHistory();

    }

    private void initilization() {
        recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void getHistory() {

        triviaDb = TriviaDb.getTrivia_dbInstance(this);
        triviaDb.open();

        Cursor cursor = triviaDb.getCompleteHistory();
        if (cursor!=null && cursor.getCount()>0)
        {
            cursor.moveToFirst();
            do {

                int slNo = cursor.getInt(cursor.getColumnIndex(TriviaDb.fldslno));
                String username = cursor.getString(cursor.getColumnIndex(TriviaDb.fldname));
                String bestplayer = cursor.getString(cursor.getColumnIndex(TriviaDb.fldbestcricketer));
                String flagcolor = cursor.getString(cursor.getColumnIndex(TriviaDb.fldflagcolor));
                String date = cursor.getString(cursor.getColumnIndex(TriviaDb.flddate));
                HistoryPojo historyPojo = new HistoryPojo(slNo,username,bestplayer,flagcolor,date);
                historyPojoArrayList.add(historyPojo);
            }
            while (cursor.moveToNext());
            cursor.close();

            historyRecyclerAdapter = new HistoryRecyclerAdapter(HistoryActivity.this, historyPojoArrayList);
            recyclerView.setAdapter(historyRecyclerAdapter);
        }
    }
}