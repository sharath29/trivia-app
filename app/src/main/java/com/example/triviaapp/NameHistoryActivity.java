package com.example.triviaapp;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.triviaapp.database.TriviaDb;

import java.text.SimpleDateFormat;
import java.util.Date;

public class NameHistoryActivity extends AppCompatActivity {

    EditText et_name;
    Button btn_Next,btn_History;
    String strName,strDate;
    TriviaDb triviaDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_history);

       initilization();

        btn_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!et_name.getText().toString().equals("")) {

                    strName=et_name.getText().toString().trim();
                    Cursor cursor = triviaDb.getUsernameCount(strName);  // check if username exist or not
                    if (cursor!=null && cursor.getCount()>0) {
                        cursor.moveToFirst();
                        int usernameCount = cursor.getInt(0);
                        if (usernameCount>0) {
                            Toast.makeText(NameHistoryActivity.this, "Username already exist", Toast.LENGTH_SHORT).show();
                        }

                        else {
                            SimpleDateFormat parseFormat = new SimpleDateFormat("E MMMM dd,yyyy hh:mm a");
                            Date date = new Date();
                            strDate = parseFormat.format(date);

                            ContentValues contentValues = new ContentValues();
                            contentValues.put(TriviaDb.fldname, strName);
                            contentValues.put(TriviaDb.fldbestcricketer, "");
                            contentValues.put(TriviaDb.fldflagcolor, "");
                            contentValues.put(TriviaDb.flddate, strDate);
                            triviaDb.insert(contentValues, TriviaDb.tbltrivia);
                            triviaDb.close();
                            startActivity(new Intent(NameHistoryActivity.this, BestPlayerActivity.class).putExtra("username", strName));
                        }


                    }

                }
                else
                    Toast.makeText(NameHistoryActivity.this, "Please enter your name to continue", Toast.LENGTH_SHORT).show();

            }
        });

        btn_History.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(NameHistoryActivity.this,HistoryActivity.class));

            }
        });

    }

    private void initilization() {

        // initilizing database here
        triviaDb = TriviaDb.getTrivia_dbInstance(this);
        triviaDb.open();

        et_name=findViewById(R.id.et_name);
        btn_Next=findViewById(R.id.btn_next);
        btn_History=findViewById(R.id.btn_history);

    }


}

