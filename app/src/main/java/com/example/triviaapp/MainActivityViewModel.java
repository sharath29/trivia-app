package com.example.triviaapp;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.triviaapp.pojo.ColorPojo;
import com.example.triviaapp.pojo.PlayersPojo;


import java.util.List;

public class MainActivityViewModel extends ViewModel {

    private MutableLiveData<List<PlayersPojo>> mBestCricketer;
    private MutableLiveData<List<ColorPojo>> mFlagColors;
    private Repository mRepo;


    // for getting players data from repository
    public void init(){
        if(mBestCricketer != null){
            return;
        }
        mRepo = Repository.getInstance();
        mBestCricketer = mRepo.getBestCricketer();
    }

    //for getting colors data from repository
    public void initColors(){
        if(mFlagColors != null){
            return;
        }
        mRepo = Repository.getInstance();
        mFlagColors = mRepo.getFlagColors();
    }

    public LiveData<List<PlayersPojo>> getBestCricketer(){
        return mBestCricketer;
    }

    public LiveData<List<ColorPojo>> getFlagColors(){
        return mFlagColors;
    }

}